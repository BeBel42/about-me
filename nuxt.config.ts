// https://nuxt.com/docs/api/configuration/nuxt-config

import tailwindcss from "@tailwindcss/vite";

export default defineNuxtConfig({
				ssr: true,
				target: "server",
				devtools: { enabled: true },
				modules: [
				 "@vueuse/nuxt",
				 "@pinia/nuxt",
				 "@nuxt/image",
				 "@nuxt/icon",
				],

				vite: {
								plugins: [tailwindcss(),],
				},

				css: ['~/assets/css/main.css'],

				image: {
								domains: ["httpdragons.com"],
				},

				components: [
								{
												path: "~/components",
												pathPrefix: false,
								},
				],

				router: {
								options: {
												scrollBehaviorType: "smooth",
								},
				},

				compatibilityDate: "2025-02-20",
});