# mlefevre.xyz
### This site is [available online!](https://mlefevre.xyz/)  

![Home page of mlefevre.xyz](https://gitlab.com/BeBel42/mlefevre.xyz/-/raw/main/screenshots/1.png)

mlefevre.xyz is my portfolio website.
It was entirely made in Vue.js, to learn a modern Javascript framework.

This site talks about
- [My life](https://mlefevre.xyz)
- [My education](https://mlefevre.xyz/education)
- [My projects](https://mlefevre.xyz/projects)

## Technical Details

### Commands
#### Installs all the dependencies
```
yarn install
```
#### Compiles and hot-reloads for development
```
yarn run dev
```
#### Compiles and minifies for production
```
yarn run build
```

### Kubernetes compatibility
mlefevre.xyz is a stateless app. It is fully compatible with Kubernetes.
