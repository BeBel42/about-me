// to keep value of isMartin globally
export const useGlobalStore = defineStore("global", () => {
	const isMartin = ref(true);
	return { isMartin };
});
