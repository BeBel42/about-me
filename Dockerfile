FROM node:lts-alpine3.19

RUN apk update && apk add yarn

WORKDIR /app

COPY . .
RUN yarn install

RUN yarn run build

ENV NITRO_PORT 80

CMD node .output/server/index.mjs

EXPOSE 80
